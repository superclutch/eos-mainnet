#!/usr/bin/env bash

docker run -it -p 8888:8888/tcp -p 9876:9876/tcp -v=./data:/etc/eosio/data superclutch/eos-mainnet-api $1